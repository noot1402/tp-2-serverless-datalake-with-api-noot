# TODO : Create the lambda role with aws_iam_role
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
# TODO : Create lambda function with aws_lambda_function

resource "aws_lambda_function" "lambda_function_name" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  function_name = "first_lambda_function"
  role          = aws_iam_role.iam_for_lambda.arn
  filename      = "empty_lambda_code.zip"
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  depends_on = [
    aws_iam_role_policy_attachment.lambda_logs,
    aws_cloudwatch_log_group.example,
  ]
}

resource "aws_cloudwatch_log_group" "example" {
  name = "/aws/lambda/first_lambda_function"
}
# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role

resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}
# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
resource "aws_iam_policy" "lambda_full_access" {
  name        = "lambda_full_access"
  path        = "/"
  description = "IAM policy for Amazonfullacess"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "s3-object-lambda:*"
            ],
            "Resource": "*"
        }
    ]
}
  EOF
}

resource "aws_iam_role_policy_attachment" "lambda_access" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.lambda_full_access.arn
}
# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.func.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.job-offer.arn
}
resource "aws_lambda_function" "func" {
  filename      = "empty_lambda_code.zip"
  function_name = "csv_created"
  role          = aws_iam_role.iam_for_lambda.arn
  handler       = "exports.example"
  runtime       = "go1.x"
}

