# TODO : Create a s3 bucket with aws_s3_bucket

resource "aws_s3_bucket" "job-offer" {
  bucket = "s3-job-offer-bucket-leroux-li-majed"
  force_destroy = true
}

# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_object

resource "aws_s3_object" "raw" {
  bucket = aws_s3_bucket.job-offer.bucket
  key = "raw/"
  source = "/dev/null"
}

# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.job-offer.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.func.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    filter_suffix       = ".csv"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}